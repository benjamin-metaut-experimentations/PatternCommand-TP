﻿using System;
using System.Collections.Generic;
using Microsoft.Data.Sqlite;

namespace Transactional_TP
{
    class Program
    {
        public static String DB_NAME = "TransactionalTP_BDD";

        public static void Main(string[] args)
        {

            Person person1 = new Person();
            person1.setId(0);
            person1.setLastName("Metaut");
            person1.setFirstName("Benjamin");
            person1.setAge(23);

            Person person2 = new Person();
            person2.setId(1);
            person2.setLastName("Dark");
            person2.setFirstName("Jeanne");
            person2.setAge(5);

            Person person3 = new Person();
            person3.setId(2);
            person3.setLastName("???");
            person3.setFirstName("???");
            person3.setAge(0);

            DBManager.getInstance.openDB(DB_NAME);

            List<ICommand> commands = new List<ICommand>();

            int userValue = 0;
            do
            {
                userValue = 0;
                Console.Clear();
                Console.WriteLine("--- Welcome to my command BDD ---");
                Console.WriteLine("Write 1 : to add a command, Write 2 : to execute all commands added");
                Console.WriteLine("Write 3 to get all persons into the DB, Write 4 : to exit");
                Console.WriteLine("Please don't enter a string or a char if you dont want to see it crash :)");
                Console.WriteLine("You have actually : " + commands.Count + " commands");

                userValue = Convert.ToInt32(Console.ReadLine());

                if (userValue == 1)
                {
                    Console.Clear();
                    userValue = 0;
                    Console.WriteLine("--- Add command ---");
                    Console.WriteLine("Write 1 : to add someone");
                    Console.WriteLine("Write 2 : to delete someone");
                    userValue = Convert.ToInt32(Console.ReadLine());
                    if(userValue == 1){
                        Console.Clear();
                        userValue = 0;
                        Console.WriteLine("--- Add person commands ---");
                        Console.WriteLine("Write 1 : to add " + person1.getFirstName());
                        Console.WriteLine("Write 2 : to add " + person2.getFirstName());
                        Console.WriteLine("Write 3 : to add " + person3.getFirstName());
                        userValue = Convert.ToInt32(Console.ReadLine());
                        switch(userValue){
                            case 1:
                                commands.Add(new AddPerson(person1));
                                break;
                            case 2:
                                commands.Add(new AddPerson(person2));
                                break;
                            case 3:
                                commands.Add(new AddPerson(person3));
                                break;
                            default:

                                break;

                        }
                        userValue = 0;
                    }
                    else if(userValue == 2){
                        Console.Clear();
                        userValue = 0;
                        Console.WriteLine("--- Delete person commands ---");
                        Console.WriteLine("Write 1 : to delete " + person1.getFirstName());
                        Console.WriteLine("Write 2 : to delete " + person2.getFirstName());
                        Console.WriteLine("Write 3 : to delete " + person3.getFirstName());
                        userValue = Convert.ToInt32(Console.ReadLine());
                        switch (userValue)
                        {
                            case 1:
                                commands.Add(new DeletePerson(person1));
                                break;
                            case 2:
                                commands.Add(new DeletePerson(person2));
                                break;
                            case 3:
                                commands.Add(new DeletePerson(person3));
                                break;
                            default:
                                break;

                        }
                        userValue = 0;
                    } 

                } 
                else if(userValue == 2){
                    TransactionCommand transactionCommand = new TransactionCommand(commands);
                    new Requester(transactionCommand).ApplyRequest();
                    Console.WriteLine("---Enter---");
                    Console.Read();

                    commands = new List<ICommand>();
                }
                else if(userValue == 3){
                    DBManager.getInstance.getPersons();
                    Console.WriteLine("---Enter---");
                    Console.Read();

                }
                else if(userValue == 4){
                    Console.WriteLine("See you next time :)");
                }
                else {
                    Console.WriteLine("Command unknown");
                    Console.WriteLine("---Enter---");
                    Console.Read();
                }

            } while (userValue != 4);

     
            DBManager.getInstance.closeDB();


        }

    }
}
