﻿using System;
using Microsoft.Data.Sqlite;

namespace Transactional_TP
{

    /*
     * This class is the Receiver
     * I prefer to do it in static unstead of instiate it because it avoid me to pass him into a concrete command
     * constructor.
     * ( I hope im not gonna lose some point for that :p look i also used a singleton :p )  
     */
    public class DBManager {
        private  static DBManager instance = null;
        private  SqliteConnection con=null;
        private  SqliteCommand sCommand;



        public static DBManager getInstance{
            get{
                if(instance == null){
                    instance = new DBManager();
                }
                return instance;
            }
        }

        public void AddPerson(Person person){
            sCommand = new SqliteCommand(String.Format("INSERT INTO persons VALUES ('{0}', '{1}', '{2}', '{3}');", person.getId(), person.getLastName(), person.getFirstName(), person.getAge()), con);
            sCommand.ExecuteReader();
        }

        public void DeletePerson(Person person){
            sCommand = new SqliteCommand(String.Format("DELETE FROM persons WHERE id == '{0}' and FirstName == '{1}' and LastName == '{2}' and age == '{3}';" , person.getId(), person.getFirstName(), person.getLastName(), person.getAge()), con);
            sCommand.ExecuteReader();

        }

        public void getPersons(){
            sCommand = new SqliteCommand("SELECT * FROM persons", con);
            using (SqliteDataReader dr = sCommand.ExecuteReader())
            {
                while (dr.Read())
                {
                    string treatment = dr[1].ToString();
                    Console.WriteLine(treatment);
                }
            }

            
        }
        public void openDB(String filename){
            con = new SqliteConnection("Filename=" + filename + ".sqlite");
            con.Open();
        }
        public  void closeDB(){
            con.Close();
        }



    } 
}
