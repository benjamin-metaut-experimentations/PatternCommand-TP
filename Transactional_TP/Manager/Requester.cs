﻿using System;
namespace Transactional_TP
{
    /*
     * This class is the invoker
     */
    public class Requester
    {
        private readonly ICommand command;

        public Requester(ICommand command){
            this.command = command;
        }

        public void ApplyRequest(){
            try{
                Console.WriteLine("Applying request..\n");
                command.Execute();
            }catch(Exception){
                
                Console.WriteLine("Error during the application \n");
                Console.WriteLine("Undo all changes.. please wait..\n");
                command.Rollback();
            }
        }
    }
}
