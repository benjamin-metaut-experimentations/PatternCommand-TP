﻿using System;
namespace Transactional_TP
{

    /*
    * This class is a concrete command : Delete
    */

    public class DeletePerson : ICommand
    {
        private Person person;
        public DeletePerson(Person person)
        {
            this.person = person;

        }

        public override void Execute()
        {
            DBManager.getInstance.DeletePerson(person);
            Console.WriteLine("Deletion : " + person.toText());

        }

        public override void Rollback()
        {
            DBManager.getInstance.AddPerson(person);
            Console.WriteLine("Add : " + person.toText());

        }
    }
}
