﻿using System;
using System.Collections.Generic;

namespace Transactional_TP
{

    /*
    * This class is a concrete command : used to play a list of some other specific command
    */
    public class TransactionCommand : ICommand
    {
        private readonly List<ICommand> commands;
        private Stack<ICommand> commandsFinished;

        public TransactionCommand(List<ICommand> commands)
        {
            this.commands = commands;
        }

        public override void Execute()
        {
            commandsFinished = new Stack<ICommand>();

            foreach(ICommand command in commands){
                command.Execute();
                commandsFinished.Push(command);
            }
            Console.WriteLine("---Success---");
            commandsFinished.Clear();
        }

        public override void Rollback()
        {
            while(commandsFinished.Count > 0){
                commandsFinished.Pop().Rollback();
            }
        }
    }
}
