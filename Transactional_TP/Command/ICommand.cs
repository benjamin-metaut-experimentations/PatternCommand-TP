﻿using System;
namespace Transactional_TP
{

    /*
    * This class is the command interface
    */
    public abstract class ICommand
    {


        public abstract void Execute();
        public abstract void Rollback();


    }
}
