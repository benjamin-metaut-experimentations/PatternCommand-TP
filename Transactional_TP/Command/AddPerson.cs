﻿using System;
namespace Transactional_TP
{
    /*
     * This class is a concrete command : Add
     */

    public class AddPerson : ICommand
    {
        
        private Person person;
        public AddPerson(Person person)
        {
            this.person = person;

        }

        public override void Execute()
        {
            DBManager.getInstance.AddPerson(person);
            Console.WriteLine("Add : " + person.toText());

        }

        public override void Rollback()
        {
            DBManager.getInstance.DeletePerson(person);
            Console.WriteLine("Deletion : " + person.toText());
        
        }
    }
}
