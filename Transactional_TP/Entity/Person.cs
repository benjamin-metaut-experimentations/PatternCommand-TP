﻿using System;
namespace Transactional_TP
{

    /*
    * This class is a Person entity
    */
    public class Person
    {
        private int id;

        private String LastName;

        private String FirstName;

        private int age;

        public int getId()
        {
            return this.id;
        }

        public String getLastName()
        {
            return this.LastName;
        }

        public String getFirstName()
        {
            return this.FirstName;
        }
        public int getAge()
        {
            return age;
        }

        public void setId(int id)
        {
            this.id = id;
        }

        public void setLastName(String LastName)
        {
            this.LastName = LastName;
        }
        public void setFirstName(String FirstName)
        {
            this.FirstName = FirstName;
        }
        public void setAge(int age)
        {
            this.age = age;
        }


        public String toText(){

            return string.Format("Person : {0}, {1}, {2}, {3}", this.id, this.FirstName, this.LastName, this.age); 
        }


    }
}
